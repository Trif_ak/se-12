package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.enumerate.CurrentStatus;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface ITaskRepository {
    void merge(@NotNull Task task) throws Exception;
    void persist(@NotNull Task task) throws Exception;
    void insert(@NotNull String name, @NotNull String id, @NotNull String projectId, @NotNull String userId, @NotNull String description, @NotNull CurrentStatus currentStatus, @NotNull Date beginDate, @NotNull Date endDate, @NotNull Date createDate) throws Exception;
    void update(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull CurrentStatus currentStatus, @NotNull Date beginDate, @NotNull Date endDate, @NotNull Date createDate) throws Exception;
    List<Task> getAll(@NotNull String projectId, @NotNull String userId) throws Exception;
    Task get(@NotNull String id, @NotNull String userId) throws Exception;
    void delete(@NotNull String id, @NotNull String userId) throws Exception;
    void deleteAllOfProject(@NotNull String projectId, @NotNull String userId) throws Exception;
    void deleteAllOfUser(@NotNull String userId) throws Exception;
    List<Task> getByPartString(@NotNull String userId, @NotNull String projectId, @NotNull String partString) throws Exception;
    List<Task> getAll() throws Exception;
}
