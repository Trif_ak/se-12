package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.*;
import ru.trifonov.tm.enumerate.CurrentStatus;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectRepository {
    void merge(@NotNull Project project) throws Exception;
    void persist(@NotNull Project project) throws Exception;
    void insert(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate, @NotNull Date createDate, @NotNull CurrentStatus currentStatus) throws Exception;
    void update(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate, @NotNull Date createDate, @NotNull CurrentStatus currentStatus) throws Exception;
    Project get(@NotNull String id, @NotNull String userId) throws Exception;
    List<Project> getAll(@NotNull String userId) throws Exception;
    void delete(@NotNull String id, @NotNull String userId) throws Exception;
    void deleteAll(@NotNull String userId) throws Exception;
    List<Project> getByPartString(@NotNull String userId, @NotNull String partString) throws Exception;
    List<Project> getAll() throws Exception;
}
