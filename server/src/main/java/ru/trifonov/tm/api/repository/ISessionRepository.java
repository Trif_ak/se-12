package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {
    void persist(@NotNull Session session) throws Exception;
    void delete(@NotNull String id) throws Exception;
    List<Session> getByUserId(@NotNull String userId) throws Exception;
    List<Session> getAll() throws Exception;
}
