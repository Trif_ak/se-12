package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user) throws Exception;

    boolean findByLogin(@NotNull String login) throws Exception;

    void merge(@NotNull User user) throws Exception;
    void insert(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType) throws Exception;
    void update(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType) throws Exception;
    User get(@NotNull String id) throws Exception;
    List<User> getAll() throws Exception;
    User existsUser(@NotNull String login, @NotNull String passwordMD5) throws Exception;
    Boolean getByLogin(@NotNull String login) throws Exception;
    void delete(@NotNull String id) throws Exception;
    void changePassword(@NotNull String userId, @NotNull String newPasswordMD5) throws Exception;
}
