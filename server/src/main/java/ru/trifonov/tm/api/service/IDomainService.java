package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.domain.Domain;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface IDomainService {
    void export (@Nullable Domain domain) throws Exception;
    void load(@Nullable Domain domain) throws Exception;
    void domainSerializable() throws Exception;
    void domainDeserializable() throws Exception;
    void domainSaveJaxbXML() throws Exception;
    void domainLoadJaxbXML() throws Exception;
    void domainSaveJaxbJSON() throws Exception;
    void domainLoadJaxbJSON() throws Exception;
    void domainSaveJacksonXML() throws Exception;
    void domainLoadJacksonXML() throws Exception;
    void domainSaveJacksonJSON() throws Exception;
    void domainLoadJacksonJSON() throws Exception;
}
