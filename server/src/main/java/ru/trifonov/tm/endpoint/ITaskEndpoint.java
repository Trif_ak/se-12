package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    void persistTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) throws Exception;

    @WebMethod
    void initTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    void updateTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    Task getTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    List<Task> getAllTaskOfProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    ) throws Exception;

    @WebMethod
    void deleteTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    void deleteAllTaskOfProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    ) throws Exception;

    @WebMethod
    void deleteAllTaskOfUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    List<Task> sortByTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull String comparatorName
    ) throws Exception;

    @WebMethod
    List<Task> getTaskByPartString(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "partString", partName = "partString") @NotNull String partString
    ) throws Exception;

    @WebMethod
    List<Task> getAllTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws Exception;
}
