package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.comparator.DateBeginComparator;
import ru.trifonov.tm.comparator.DateCreateComparator;
import ru.trifonov.tm.comparator.DateEndComparator;
import ru.trifonov.tm.comparator.StatusComparator;
import ru.trifonov.tm.entity.ComparableEntity;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

abstract class ComparatorService extends AbstractService {
    @NotNull private static final Map<String, Comparator<ComparableEntity>> comparators = new LinkedHashMap<>();
    @NotNull private static final Comparator<ComparableEntity> dateBeginComparator = new DateBeginComparator();
    @NotNull private static final Comparator<ComparableEntity> dateEndComparator = new DateEndComparator();
    @NotNull private static final Comparator<ComparableEntity> dateCreateComparator = new DateCreateComparator();
    @NotNull private static final Comparator<ComparableEntity> statusComparator = new StatusComparator();

    static {
        comparators.put("date-create", dateCreateComparator);
        comparators.put("date-begin", dateBeginComparator);
        comparators.put("date-end", dateEndComparator);
        comparators.put("status", statusComparator);
    }

    Comparator<ComparableEntity> getComparator(@NotNull final String comparatorName) {
        return comparators.get(comparatorName);
    }
}
