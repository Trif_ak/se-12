package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IUserRepository;
import ru.trifonov.tm.api.service.IUserService;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.HashUtil;
import ru.trifonov.tm.util.IdUtil;

import java.util.List;

public final class UserService extends ComparatorService implements IUserService {
    @NotNull private IUserRepository userRepository;

    public UserService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if (user == null) throw new NullPointerException("Enter correct data");
        userRepository.persist(user);
    }

    @Override
    public User existsUser(
            @Nullable final String login, @Nullable final String password
    ) throws Exception {
        if (login == null || login.trim().isEmpty()) return null;
        if (password == null || password.trim().isEmpty()) return null;
        return userRepository.existsUser(login, HashUtil.md5(password));
    }

    @Override
    public void registrationUser(
            @Nullable final String login, @Nullable final String password
    ) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!userRepository.findByLogin(login)) throw new Exception("Login already exist.");
//        if (!userRepository.getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.REGULAR_USER);
        userRepository.persist(user);
    }

    @Override
    public void registrationAdmin(
            @Nullable final String login, @Nullable final String password
    ) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!userRepository.findByLogin(login)) throw new Exception("Login already exist.");
//        if (!userRepository.getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.ADMIN);
        userRepository.persist(user);
    }

    @Override
    public void update(
            @Nullable final String id, @Nullable final String login,
            @Nullable final String password, @Nullable final RoleType roleType
    ) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (roleType == null) throw new NullPointerException("Enter correct data");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.ADMIN);
        userRepository.merge(user);
    }

    @Override
    public List<User> getAll() throws Exception {
        return userRepository.getAll();
    }

    @Override
    public User get(@Nullable final String id) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return userRepository.get(id);
    }

    @Override
    public void delete(@Nullable final String id) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        userRepository.delete(id);
    }


    @Override
    public void load(@Nullable final List<User> users
    ) throws Exception {
        if (users == null) return;
        for (@NotNull final User user : users) {
            userRepository.persist(user);
        }
    }

    @Override
    public void addUser() throws Exception {
        @NotNull final User admin = new User();
//        @NotNull final String adminLogin = "admin";
        admin.setLogin("admin");
        admin.setPasswordMD5(HashUtil.md5("admin"));
        admin.setRoleType(RoleType.ADMIN);
        if (userRepository.findByLogin(admin.getLogin())) throw new Exception("Login already exist.");
        userRepository.persist(admin);

        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPasswordMD5(HashUtil.md5("user"));
        user.setRoleType(RoleType.REGULAR_USER);
        if (userRepository.findByLogin(user.getLogin())) throw new Exception("Login already exist.");
        userRepository.persist(user);
    }

    @Override
    public void changePassword(
            @Nullable final String userId, @Nullable final String newPassword
    ) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (newPassword == null || newPassword.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final String newPasswordMD5 = HashUtil.md5(newPassword);
        userRepository.changePassword(userId, newPasswordMD5);
    }
}