package ru.trifonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ITaskRepository;
import ru.trifonov.tm.api.service.ITaskService;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.util.IdUtil;

import java.text.ParseException;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends ComparatorService implements ITaskService {
    private @NotNull ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if (task == null) throw new NullPointerException("Enter correct data");
        taskRepository.persist(task);
    }

    @SneakyThrows
    @Override
    public void init(
            @Nullable final String name, @Nullable final String projectId,
            @Nullable final String userId, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Task task = new Task(name, IdUtil.getUUID(), projectId, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        taskRepository.persist(task);
    }

    @Override
    public void update(
            @Nullable final String name, @Nullable final String id,
            @Nullable final String projectId, @Nullable final String userId,
            @Nullable final String description, @Nullable final String beginDate,
            @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Task task = new Task(name, id, projectId, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        taskRepository.merge(task);
    }

    @Override
    public Task get(
            @Nullable final String id, @Nullable final String userId
    ) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.get(id, userId);
    }

    @Override
    public List<Task> getAllOfProject (
            @Nullable final String projectId, @Nullable final String userId
    ) throws Exception {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.getAll(projectId, userId);
    }

    @Override
    public void delete(
            @Nullable final String id, @Nullable final String userId
    ) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.delete(id, userId);
    }

    @Override
    public void deleteAllOfProject(
            @Nullable final String projectId, @Nullable final String userId
    ) throws Exception {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.deleteAllOfProject(projectId, userId);
    }

    @Override
    public void deleteAllOfUser(@Nullable final String userId) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.deleteAllOfUser(userId);
    }

    @Override
    public List<Task> sortBy(
            @Nullable final String projectId, @Nullable final String userId,
            @Nullable final String comparatorName
    ) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Task> tasks = getAllOfProject(projectId, userId);
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> getByPartString(
            @Nullable final String userId, @Nullable final String projectId,
            @Nullable final String partString
    ) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.getByPartString(userId, projectId, partString);
    }

    @Override
    public List<Task> getAll() throws Exception {
        return taskRepository.getAll();
    }

    @Override
    public void load(@Nullable final List<Task> tasks) throws Exception {
        if (tasks == null) return;
        for (@NotNull final Task task : tasks) {
            taskRepository.persist(task);
        }
    }
}