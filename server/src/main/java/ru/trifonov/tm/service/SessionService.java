package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.api.repository.ISessionRepository;
import ru.trifonov.tm.api.service.ISessionService;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.IdUtil;
import ru.trifonov.tm.util.SignatureUtil;

import java.util.Arrays;
import java.util.List;

public final class SessionService extends AbstractService implements ISessionService {
    List<RoleType> roles = Arrays.asList(RoleType.REGULAR_USER, RoleType.ADMIN);
    @NotNull private ISessionRepository sessionRepository;
    @NotNull private ServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final ServiceLocator serviceLocator
    ) {
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    public Session openSession(
            @Nullable final String login, @Nullable final String password
    ) throws Exception {
        if (login == null || login.trim().isEmpty()) return null;
        if (password == null || password.trim().isEmpty()) return null;
        @Nullable User checkUser = serviceLocator.getUserService().existsUser(login, password);
        if (checkUser == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(checkUser.getId());
        session.setRole(checkUser.getRoleType());
        @NotNull final String salt = serviceLocator.getPropertyService().getServerSalt();
        @NotNull final Integer cycle = serviceLocator.getPropertyService().getServerCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        sessionRepository.persist(session);
        return session;
    }

    @Override
    public void closeSession(@Nullable final Session session) throws Exception {
        if (session == null) throw new NullPointerException("Enter correct data");
        sessionRepository.delete(session.getId());
    }

    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if (session == null) throw new NullPointerException("Enter correct data");
        sessionRepository.persist(session);
    }

    @Override
    public List<Session> getByUserId(@NotNull final Session session) throws Exception {
        validate(session);
        return sessionRepository.getByUserId(session.getUserId());
    }

    @Override
    public void validateAdmin(@Nullable final Session session) throws Exception {
        if (session == null) throw new NullPointerException("Enter correct data");
        if (session.getSignature() == null || session.getSignature().trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!session.getRole().equals(RoleType.ADMIN)) throw new Exception("Invalid session.");
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new NullPointerException("Enter correct data");
        @NotNull final String signatureSession = session.getSignature();
        @Nullable final String signatureTemp = sign(temp).getSignature();
        if (!signatureSession.equals(signatureTemp)) throw new NullPointerException("Enter correct data");
        @NotNull final long timeStampOfSession = session.getTimeStamp();
        @NotNull final long currentTime = System.currentTimeMillis();
        if (currentTime - timeStampOfSession > 900000) throw new Exception("Session time is out. Please, LOG IN");
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new NullPointerException("Enter correct data");
        if (session.getSignature() == null || session.getSignature().trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!roles.contains(session.getRole())) throw new Exception("Invalid session.");
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new NullPointerException("Enter correct data");
        @NotNull final String signatureSession = session.getSignature();
        @Nullable final String signatureTemp = sign(temp).getSignature();
        if (!signatureSession.equals(signatureTemp)) throw new NullPointerException("Enter correct data");
        @NotNull final long timeStampOfSession = session.getTimeStamp();
        @NotNull final long currentTime = System.currentTimeMillis();
        if (currentTime - timeStampOfSession > 900000) throw new Exception("Session time is out. Please, LOG IN");
    }

    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) throw new NullPointerException("Enter correct data");
        session.setSignature(null);
        @Nullable final String salt = serviceLocator.getPropertyService().getServerSalt();
        @Nullable final Integer cycle = serviceLocator.getPropertyService().getServerCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public List<Session> getAll() throws Exception {
        return sessionRepository.getAll();
    }

    @Override
    public void load(@Nullable final List<Session> sessions) throws Exception {
        if (sessions == null) return;
        for (@NotNull final Session session : sessions) {
            sessionRepository.persist(session);
        }
    }
}
