package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {
    @NotNull
    private final String APP_PROPERTY = "/application.properties";
    @NotNull
    private final String DBMS_PROPERTY = "/dataBase.properties";
    @NotNull
    private final Properties appProperties = new Properties();
    @NotNull
    private final Properties dbmsProperties = new Properties();

    @Override
    public void init() throws IOException {
        InputStream inputStreamApp = Properties.class.getResourceAsStream(APP_PROPERTY);
        appProperties.load(inputStreamApp);
        InputStream inputStreamDBMS = Properties.class.getResourceAsStream(DBMS_PROPERTY);
        dbmsProperties.load(inputStreamDBMS);
    }

    @Override
    public String getURL() {
        return dbmsProperties.getProperty("url");
    }

    @Override
    public String getUsername() {
        return dbmsProperties.getProperty("username");
    }

    @Override
    public String getPassword() {
        return dbmsProperties.getProperty("password");
    }

    @Override
    public String getServerHost() {
        return appProperties.getProperty("server.host");
    }

    @Override
    public String getServerPort() {
        return appProperties.getProperty("server.port");
    }

    @Override
    public String getServerSalt() {
        return appProperties.getProperty("server.salt");
    }

    @Override
    public Integer getServerCycle() {
        return Integer.parseInt(appProperties.getProperty("server.cycle"));
    }
}
