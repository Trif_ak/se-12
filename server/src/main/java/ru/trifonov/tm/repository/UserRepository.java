package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IUserRepository;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @NotNull
    private final Connection connection;

    public UserRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public void persist(@NotNull final User user) throws Exception {
        @NotNull final String id = user.getId();
        @NotNull final String login = user.getLogin();
        @NotNull final String passwordHash = user.getPasswordMD5();
        @NotNull final String role = user.getRoleType().toString();
        @NotNull final String query =
                "INSERT INTO user (id, login, passwordHash, role) VALUES (?, ?, ?, ?)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, login);
        preparedStatement.setString(3, passwordHash);
        preparedStatement.setString(4, role);
        preparedStatement.executeUpdate(query);
        preparedStatement.close();
    }

    @Override
    public boolean findByLogin(@NotNull final String login) throws Exception {
        @NotNull final String query = "SELECT * FROM user WHERE login = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, login);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull String result = "";
        while (resultSet.next()) {
             result = resultSet.getString("login");
        }
        return login.equals(result);
    }

    @Override
    public void merge(@NotNull final User user) throws Exception {
        @NotNull final String id = user.getId();
        @NotNull final String query =
                "SELECT * FROM user WHERE id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull String result = "";
        while (resultSet.next()) {
            result = resultSet.getString("id");
        }
        if (result.equals(id)) {
            update(user.getId(), user.getLogin(), user.getPasswordMD5(), user.getRoleType());
        } else {
            insert(user.getId(), user.getLogin(), user.getPasswordMD5(), user.getRoleType());
        }
    }

    @Override
    public void insert(
            @NotNull final String id, @NotNull final String login,
            @NotNull final String passwordHash, @NotNull final RoleType roleType
    ) throws Exception {
        @NotNull final String role = roleType.toString();
        @NotNull final String query =
                "INSERT INTO user (id, login, passwordHash, role) " +
                "VALUES (1, 2, 3, 4)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, login);
        preparedStatement.setString(3, passwordHash);
        preparedStatement.setString(4, role);
        preparedStatement.executeUpdate(query);
        preparedStatement.close();
    }

    @Override
    public void update(
            @NotNull final String id, @NotNull final String login,
            @NotNull final String passwordHash, @NotNull final RoleType roleType
    ) throws Exception {
        @Nullable final Statement statement = connection.createStatement();
        if (statement == null) throw new Exception("Bad connection. Please, try again.");
        @NotNull final String role = roleType.toString();
        @NotNull final String query =
                "UPDATE user SET login = '" + login + "', passwordHash = '" + passwordHash + "', role = '" + role +
                        "' WHERE id = '" + id + "';";
        statement.executeUpdate(query);
        statement.close();
    }

    @Override
    public User get(@NotNull final String id) throws Exception {
        @NotNull final String query = "SELECT * FROM user WHERE id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull final User user = new User();
        while (resultSet.next()) {
            user.setId(resultSet.getString("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPasswordMD5(resultSet.getString("passwordHash"));
            user.setRoleType(RoleType.valueOf(resultSet.getString("role")));
        }
        return user;
    }

    @Override
    public List<User> getAll() throws Exception {
        @NotNull final List<User> output = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM user;";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull final User user = new User();
            user.setId(resultSet.getString("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPasswordMD5(resultSet.getString("passwordHash"));
            user.setRoleType(RoleType.valueOf(resultSet.getString("role")));
            output.add(user);
        }
        if (output.isEmpty()) throw new NullPointerException("Not found users. Enter correct data");
        return output;
    }

    @Override
    public User existsUser(
            @NotNull final String login, @NotNull final String passwordMD5
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM user WHERE login = ? AND passwordHash = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, passwordMD5);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull final User user = new User();
        while (resultSet.next()) {
            user.setId(resultSet.getString("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPasswordMD5(resultSet.getString("passwordHash"));
            user.setRoleType(RoleType.valueOf(resultSet.getString("role")));
        }
        return user;
    }

    @Override
    public Boolean getByLogin(@NotNull final String login) throws Exception {
        @NotNull final List<String> logins = new ArrayList<>();
        @NotNull final String query = "SELECT login FROM user;";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull final String loginFromDB = resultSet.getString("login");
            logins.add(loginFromDB);
        }
        for (@NotNull final String loginFromDB : logins) {
            if (loginFromDB.equals(login)) return true;
        }
        return false;
    }

    @Override
    public void delete(@NotNull final String id) throws Exception {
        @NotNull final String query = "DELETE FROM user WHERE id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate(query);
        preparedStatement.close();
    }

    @Override
    public void changePassword(
            @NotNull final String id, @NotNull final String newPasswordMD5
    ) throws Exception {
        @NotNull final String query = "UPDATE user SET passwordHash = ? WHERE id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, newPasswordMD5);
        preparedStatement.executeUpdate(query);
        preparedStatement.close();
    }
}

