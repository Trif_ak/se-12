package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IProjectRepository;
import ru.trifonov.tm.entity.*;
import ru.trifonov.tm.enumerate.CurrentStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    @NotNull
    private final Connection connection;

    public ProjectRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public void persist(@NotNull final Project project) throws Exception {
        @NotNull final String id = project.getId();
        @NotNull final String name = project.getName();
        @NotNull final String userId = project.getUserId();
        @NotNull final String description = project.getDescription();
        @NotNull final String status = project.getStatus().toString();
        @NotNull final String beginDate = dateFormat.format(project.getBeginDate());
        @NotNull final String endDate = dateFormat.format(project.getEndDate());
        @NotNull final String createDate = dateFormat.format(project.getCreateDate());
        @NotNull final String query =
                "INSERT INTO project (id, user_id, name, description, status, " +
                "begin_date, end_date, create_date) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, userId);
        preparedStatement.setString(3, name);
        preparedStatement.setString(4, description);
        preparedStatement.setString(5, status);
        preparedStatement.setString(6, beginDate);
        preparedStatement.setString(7, endDate);
        preparedStatement.setString(8, createDate);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void merge(@NotNull final Project project) throws Exception {
        @NotNull final String id = project.getId();
        @NotNull final String query =
                "SELECT * FROM project WHERE id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull String result = "";
        while (resultSet.next()) {
            result = resultSet.getString("id");
        }
        if (result.equals(id)) {
            update(project.getName(), project.getId(), project.getUserId(), project.getDescription(),
                    project.getBeginDate(), project.getEndDate(), project.getCreateDate(), project.getStatus());
        } else {
            insert(project.getName(), project.getId(), project.getUserId(), project.getDescription(),
                    project.getBeginDate(), project.getEndDate(), project.getCreateDate(), project.getStatus());
        }
    }

    @Override
    public void insert(
            @NotNull final String name, @NotNull final String id,
            @NotNull final String userId, @NotNull final String description,
            @NotNull final Date beginDate, @NotNull final Date endDate,
            @NotNull final Date createDate, @NotNull final CurrentStatus currentStatus
    ) throws Exception {
        @NotNull final String correctBeginDate = dateFormat.format(beginDate);
        @NotNull final String correctEndDate = dateFormat.format(endDate);
        @NotNull final String correctCreateDate = dateFormat.format(createDate);
        @NotNull final String correctStatus = currentStatus.toString();
        @NotNull final String query =
                "INSERT INTO project (id, user_id, name, description, status, " +
                "begin_date, end_date, create_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, userId);
        preparedStatement.setString(3, name);
        preparedStatement.setString(4, description);
        preparedStatement.setString(5, correctStatus);
        preparedStatement.setString(6, correctBeginDate);
        preparedStatement.setString(7, correctEndDate);
        preparedStatement.setString(8, correctCreateDate);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void update(
            @NotNull final String name, @NotNull final String id,
            @NotNull final String userId, @NotNull final String description,
            @NotNull final Date beginDate, @NotNull final Date endDate,
            @NotNull final Date createDate, @NotNull CurrentStatus currentStatus
    ) throws Exception {
        @NotNull final String correctBeginDate = dateFormat.format(beginDate);
        @NotNull final String correctEndDate = dateFormat.format(endDate);
        @NotNull final String correctCreateDate = dateFormat.format(createDate);
        @NotNull final String correctStatus = currentStatus.toString();
        @NotNull final String query =
                "UPDATE project SET name = ?, description = ?, status = ?, " +
                "begin_date = ?, end_date = ?, create_date = ? WHERE id = ? AND user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, description);
        preparedStatement.setString(3, correctStatus);
        preparedStatement.setString(4, correctBeginDate);
        preparedStatement.setString(5, correctEndDate);
        preparedStatement.setString(6, correctCreateDate);
        preparedStatement.setString(7, id);
        preparedStatement.setString(8, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public Project get(
            @NotNull final String id, @NotNull final String userId
    ) throws Exception {
        @NotNull final String query =
                "SELECT * FROM project WHERE id = ? AND user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, userId);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull final Project project = new Project();
        while (resultSet.next()) {
            project.setId(id);
            project.setUserId(userId);
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setStatus(CurrentStatus.valueOf(resultSet.getString("status")));
            project.setBeginDate(resultSet.getDate("begin_date"));
            project.setEndDate(resultSet.getDate("end_date"));
            project.setCreateDate(resultSet.getDate("create_date"));
        }
        return project;
    }

    @Override
    public List<Project> getAll(@NotNull final String userId) throws Exception {
        @NotNull final List<Project> output = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM project WHERE user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, userId);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString("id"));
            project.setUserId(resultSet.getString("user_id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setStatus(CurrentStatus.valueOf(resultSet.getString("status")));
            project.setBeginDate(resultSet.getDate("begin_date"));
            project.setEndDate(resultSet.getDate("end_date"));
            project.setCreateDate(resultSet.getDate("create_date"));
            output.add(project);
        }
        return output;
    }


    @Override
    public void delete(
            @NotNull final String id, @NotNull final String userId
    ) throws Exception {
        @NotNull final String query =
                "DELETE FROM project WHERE id = ? AND user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void deleteAll(@NotNull final String userId) throws Exception {
        @NotNull final String query = "DELETE FROM project WHERE user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public List<Project> getByPartString(
            @NotNull final String userId, @NotNull final String partString
    ) throws Exception {
        @Nullable final List<Project> projects = getAll(userId);
        if (projects.isEmpty()) throw new NullPointerException("Not found projects. Enter correct date");
        @Nullable final List<Project> output = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (project.getName().contains(partString) || project.getDescription().contains(partString)) {
                output.add(project);
            }
        }
        if (output.isEmpty()) throw new NullPointerException("Not found projects. Enter correct date");
        return output;
    }

    @Override
    public List<Project> getAll() throws Exception {
        @NotNull final List<Project> output = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM project;";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull final Project project = new Project();
            project.setId(resultSet.getString("id"));
            project.setUserId(resultSet.getString("user_id"));
            project.setName(resultSet.getString("name"));
            project.setDescription(resultSet.getString("description"));
            project.setStatus(CurrentStatus.valueOf(resultSet.getString("status")));
            project.setBeginDate(resultSet.getDate("begin_date"));
            project.setEndDate(resultSet.getDate("end_date"));
            project.setCreateDate(resultSet.getDate("create_date"));
            output.add(project);
        }
        return output;
    }
}
