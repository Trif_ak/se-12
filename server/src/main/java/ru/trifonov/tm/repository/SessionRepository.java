package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ISessionRepository;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.enumerate.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @NotNull
    private final Connection connection;

    public SessionRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public void persist(@NotNull final Session session) throws Exception {
        @NotNull final String id = session.getId();
        @NotNull final String user_id = session.getUserId();
        @NotNull final Long timeStamp = session.getTimeStamp();
        @NotNull final String signature = session.getSignature();
        @NotNull final String role = session.getRole().toString();
        @NotNull final String query =
                "INSERT INTO session (id, user_id, signature, timestamp, role) " +
                "VALUES (?, ?, ?, ?, ?)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, user_id);
        preparedStatement.setString(3, signature);
        preparedStatement.setLong(4, timeStamp);
        preparedStatement.setString(5, role);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void delete(@NotNull final String id) throws Exception {
        @NotNull final String query = "DELETE FROM session WHERE id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public List<Session> getByUserId(@NotNull final String userId) throws Exception {
        @Nullable final List<Session> sessions = new ArrayList<>();
        @Nullable final String query =
                "SELECT * FROM session WHERE user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, userId);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull Session session = new Session();
            session.setId(resultSet.getString("id"));
            session.setUserId(resultSet.getString("user_id"));
            session.setTimeStamp(Long.parseLong(resultSet.getString("timestamp")));
            session.setSignature(resultSet.getString("signature"));
            session.setRole(RoleType.valueOf(resultSet.getString("role")));
            sessions.add(session);
        }
        if (sessions.isEmpty())  throw new NullPointerException("Not found sessions. Enter correct date");
        return sessions;
    }

    @Override
    public List<Session> getAll() throws Exception {
        @Nullable final List<Session> sessions = new ArrayList<>();
        @Nullable final String query = "SELECT * FROM session";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull Session session = new Session();
            session.setId(resultSet.getString("id"));
            session.setUserId(resultSet.getString("user_id"));
            session.setTimeStamp(Long.parseLong(resultSet.getString("timestamp")));
            session.setSignature(resultSet.getString("signature"));
            session.setRole(RoleType.valueOf(resultSet.getString("role")));
            sessions.add(session);
        }
        return sessions;
    }
}