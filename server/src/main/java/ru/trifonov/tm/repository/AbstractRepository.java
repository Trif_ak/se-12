package ru.trifonov.tm.repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractRepository<T> {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    final Map<String, T> entities = new LinkedHashMap<>();
}
