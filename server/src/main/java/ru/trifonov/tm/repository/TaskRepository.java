package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ITaskRepository;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.enumerate.CurrentStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    @NotNull
    private final Connection connection;

    public TaskRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public void persist(@NotNull final Task task) throws Exception {
        @NotNull final String id = task.getId();
        @NotNull final String name = task.getName();
        @NotNull final String projectId = task.getProjectId();
        @NotNull final String userId = task.getUserId();
        @NotNull final String description = task.getDescription();
        @NotNull final String status = task.getStatus().toString();
        @NotNull final String beginDate = dateFormat.format(task.getBeginDate());
        @NotNull final String endDate = dateFormat.format(task.getEndDate());
        @NotNull final String createDate = dateFormat.format(task.getCreateDate());
        @NotNull final String query =
                "INSERT INTO task (id, project_id, user_id, name, description, status," +
                " begin_date, end_date, create_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, projectId);
        preparedStatement.setString(3, userId);
        preparedStatement.setString(4, name);
        preparedStatement.setString(5, description);
        preparedStatement.setString(6, status);
        preparedStatement.setString(7, beginDate);
        preparedStatement.setString(8, endDate);
        preparedStatement.setString(9, createDate);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void merge(@NotNull final Task task) throws Exception {
        @NotNull final String id = task.getId();
        @NotNull final String query =
                "SELECT * FROM task WHERE id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery(query);
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull String result = "";
        while (resultSet.next()) {
            result = resultSet.getString("id");
        }
        if (result.equals(id)) {
            System.out.println("update");
            update(task.getName(), task.getId(), task.getUserId(), task.getDescription(),
                    task.getStatus(), task.getBeginDate(), task.getEndDate(), task.getCreateDate());
        } else {
            System.out.println("insert");
            insert(task.getName(), task.getId(), task.getProjectId(), task.getUserId(), task.getDescription(),
                    task.getStatus(), task.getBeginDate(), task.getEndDate(), task.getCreateDate());
        }
    }

    @Override
    public void insert(
            @NotNull final String name, @NotNull final String id,
            @NotNull final String projectId, @NotNull final String userId,
            @NotNull final String description, @NotNull final CurrentStatus currentStatus,
            @NotNull final Date beginDate, @NotNull final Date endDate, @NotNull final Date createDate
    ) throws Exception {
        @NotNull final String status = currentStatus.toString();
        @NotNull final String correctBeginDate = dateFormat.format(beginDate);
        @NotNull final String correctEndDate = dateFormat.format(endDate);
        @NotNull final String correctCreateDate = dateFormat.format(createDate);
        @NotNull final String query =
                "INSERT INTO task (id, project_id, user_id, name, description, status, begin_date, end_date, create_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, projectId);
        preparedStatement.setString(3, userId);
        preparedStatement.setString(4, name);
        preparedStatement.setString(5, description);
        preparedStatement.setString(6, status);
        preparedStatement.setString(7, correctBeginDate);
        preparedStatement.setString(8, correctEndDate);
        preparedStatement.setString(9, correctCreateDate);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void update(
            @NotNull final String name, @NotNull final String id, @NotNull final String userId,
            @NotNull final String description, @NotNull final CurrentStatus currentStatus,
            @NotNull final Date beginDate, @NotNull final Date endDate, @NotNull final Date createDate
    ) throws Exception {
        @NotNull final String status = currentStatus.toString();
        @NotNull final String correctBeginDate = dateFormat.format(beginDate);
        @NotNull final String correctEndDate = dateFormat.format(endDate);
        @NotNull final String correctCreateDate = dateFormat.format(createDate);
        @NotNull final String query =
                "UPDATE task SET name = ?, description = ?, status = ?, " +
                "begin_date = ?, end_date = ?, create_date = ? WHERE id = ? AND user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, description);
        preparedStatement.setString(3, status);
        preparedStatement.setString(4, correctBeginDate);
        preparedStatement.setString(5, correctEndDate);
        preparedStatement.setString(6,correctCreateDate);
        preparedStatement.setString(7, id);
        preparedStatement.setString(8, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public Task get(
            @NotNull final String id, @NotNull final String userId
    ) throws Exception {
        @NotNull final String query =
                "SELECT * FROM task WHERE id = ? AND user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, userId);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        @NotNull final Task task = new Task();
        while (resultSet.next()) {
            task.setId(id);
            task.setUserId(userId);
            task.setProjectId(resultSet.getString("project_id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setStatus(CurrentStatus.valueOf(resultSet.getString("status")));
            task.setBeginDate(resultSet.getDate("begin_date"));
            task.setEndDate(resultSet.getDate("end_date"));
            task.setCreateDate(resultSet.getDate("create_date"));
        }
        return task;
    }

    @Override
    public List<Task> getAll(
            @NotNull final String projectId, @NotNull final String userId
    ) throws Exception {
        @NotNull final List<Task> output = new ArrayList<>();
        @NotNull final String query =
                "SELECT * FROM task WHERE user_id = ? AND project_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setUserId(resultSet.getString("user_id"));
            task.setProjectId(resultSet.getString("project_id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setStatus(CurrentStatus.valueOf(resultSet.getString("status")));
            task.setBeginDate(resultSet.getDate("begin_date"));
            task.setEndDate(resultSet.getDate("end_date"));
            task.setCreateDate(resultSet.getDate("create_date"));
            output.add(task);
        }
        return output;
    }

    @Override
    public void delete(
            @NotNull final String id, @NotNull final String userId
    ) throws Exception {
        @NotNull final String query =
                "DELETE FROM task WHERE id = ? AND user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void deleteAllOfProject(
            @NotNull final String projectId, @NotNull final String userId
    ) throws Exception {
        @NotNull final String query =
                "DELETE FROM project WHERE project_id = ? AND user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, projectId);
        preparedStatement.setString(2, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void deleteAllOfUser(@NotNull final String userId) throws Exception {
        @NotNull final String query =
                "DELETE FROM project WHERE user_id = ?";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public List<Task> getByPartString(
            @NotNull final String userId, @NotNull final String projectId,
            @NotNull final String partString
    ) throws Exception {
        @Nullable final List<Task> tasks = getAll(userId, projectId);
        if (tasks.isEmpty()) throw new NullPointerException("Not found tasks. Enter correct date");

        @Nullable final List<Task> output = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (task.getName().contains(partString) || task.getDescription().contains(partString)) {
                output.add(task);
            }
        }
        if (output.isEmpty()) throw new NullPointerException("Not found tasks. Enter correct date");
        return output;
    }

    @Override
    public List<Task> getAll() throws Exception {
        @NotNull final List<Task> output = new ArrayList<>();
        @NotNull final String query =
                "SELECT * FROM task;";
        @Nullable final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if (preparedStatement == null) throw new Exception("Bad connection. Please, try again.");
        @Nullable ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet == null) throw new Exception("Not found. Please, try again.");
        while (resultSet.next()) {
            @NotNull final Task task = new Task();
            task.setId(resultSet.getString("id"));
            task.setUserId(resultSet.getString("user_id"));
            task.setProjectId(resultSet.getString("project_id"));
            task.setName(resultSet.getString("name"));
            task.setDescription(resultSet.getString("description"));
            task.setStatus(CurrentStatus.valueOf(resultSet.getString("status")));
            task.setBeginDate(resultSet.getDate("begin_date"));
            task.setEndDate(resultSet.getDate("end_date"));
            task.setCreateDate(resultSet.getDate("create_date"));
            output.add(task);
        }
        return output;
    }
}