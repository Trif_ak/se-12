package ru.trifonov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.*;
import ru.trifonov.tm.api.service.*;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.repository.*;
import ru.trifonov.tm.service.*;

import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {
    @NotNull private final IPropertyService propertyService = new PropertyService();
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository(getConnection());
    @NotNull private final ITaskRepository taskRepository = new TaskRepository(getConnection());
    @NotNull private final IUserRepository userRepository = new UserRepository(getConnection());
    @NotNull private final ISessionRepository sessionRepository = new SessionRepository(getConnection());
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final ISessionService sessionService = new SessionService(sessionRepository, this);
    @NotNull private final IDomainService domainService = new DomainService(this);
    @NotNull private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    public void start() {
        try {
            init();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void init() throws Exception {
        initProperty();
        initEndpoint();
        initUsers();
    }

    private void initUsers() throws Exception {
        userService.addUser();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    private void initProperty() throws IOException {
        propertyService.init();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        publisher(wsdl, endpoint);
    }

    private void publisher(@NotNull final String wsdl, @NotNull final Object endpoint) {
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private Connection getConnection() {
        initProperty();
        @NotNull final String url = propertyService.getURL();
        @NotNull final String username = propertyService.getUsername();
        @NotNull final String password = propertyService.getPassword();
        return DriverManager.getConnection(url, username, password);
    }
}