package ru.trifonov.tm.terminal.system;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class ExitCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": program exit";
    }

    @Override
    public void execute() {
        System.out.println("[PROGRAM EXIT]");
        System.exit(0);
    }
}
