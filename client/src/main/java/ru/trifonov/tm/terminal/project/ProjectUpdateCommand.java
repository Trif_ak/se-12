package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": update select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT UPDATE]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the project you want to update");
        @Nullable final String id = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new name");
        @Nullable final String name = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new description");
        @Nullable final String description = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new begin date. Date format DD.MM.YYYY");
        @Nullable final String beginDate = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter new end date. Date format DD.MM.YYYY");
        @Nullable final String endDate = bootstrap.getTerminalService().getInCommand();
        bootstrap.getProjectEndpoint().updateProject(currentSession, name, id, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
