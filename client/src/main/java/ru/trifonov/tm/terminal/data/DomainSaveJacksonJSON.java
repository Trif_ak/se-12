package ru.trifonov.tm.terminal.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class DomainSaveJacksonJSON extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "domain-saveJackJS";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to json with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN SAVE OF JACKSON JSON]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getDomainEndpoint().domainSaveJacksonJSON(currentSession);
        System.out.println("[OK]");
    }
}
