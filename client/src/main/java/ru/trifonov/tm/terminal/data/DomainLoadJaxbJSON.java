package ru.trifonov.tm.terminal.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class DomainLoadJaxbJSON extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "domain-loadJaxJS";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": load domain to json with jax-b";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN LOAD OF JAXB JSON]");
        @Nullable final Session currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getDomainEndpoint().domainLoadJaxbJSON(currentSession);
        System.out.println("[OK]");
    }
}
